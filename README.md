This is a noteexam practice to prepare myself to take OCA Java 7

Reference:
* Sierra, Kathy, and Bert Bates. OCA/OCP Java SE 7 Programmer I & II Study Guide (exams 1Z0-803 & 1Z0-804). New York: McGraw-Hill Education, 2015.
