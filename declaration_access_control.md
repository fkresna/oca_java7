### Identifiers
* Identifiers can begin with a letter, an underscore, or acurrency character.
* After the first character, identifiers can also include digits.
* Identifiers can be of any length.

### Executable Java Files and main()
* You can compile and execute Java programs using the command-line program *javac* and *java*,respectively. Both programs support a variety of command-line options
* The only version of *main()* methods with special powers are those versions with method signatures equivalent to *public statis void main(String[] args)*.
* *main()* can be overload.

### Imports
* An *import* statement's only job is to save keystrokes.
* You can use an asterik (*) to search through the contents of a single package.
* Although refrerrd to as "statuc imports," the syntax is *import static...*
* You can import API classes and/or custom classes.

### Source File Declaration Rules
* A source file can have only one *public* class
* If the cource file contains a *public* class, the filename must match the *public* class name.
* A file can have only one *package* statement, but it can have multiple *imports*.
* The *package* statement (if any) must be the first (noncomment) line in a source file.
* The *import* statements (if any) must come after the *package* and before the class declaration.
* If there is no *package* statement, *import* statements must be the first (noncomment) statements in the source file.
* *package* and *import* statements apply to all classes inthe file.
* A file can have more than one nonpublic class.
* Files with no *public* classes have no naming restrictions.

### Class Access Modifiers
* There are thress access modifiers: *public*, *protected*, and *private*
* Therea are four access levels: *public*, *protected*, default, and *private*.
* Classes can have only *public* or default access.
* A class with default access can be seen oly by clasesses within the same package.
A class with *public* access can be seen by all classes from all packages.
* Class visibility revolves around whether code in one class can
	* Create an instance of another class
	* EExtend (or subclass) another class
	* Access methods and variables of another class

### Class Modifiers (Nonaccess)
* Classes can also be modified with *final*, *abstract*, or *strictfp*
* A class cannot be both *final* and *abstract*
* A *final* class cannot be subclassed
* An *abstract* class cannot be instantiated
* A single *abstract* method in a class means the whole class must be *abstract*
* An *abstract* class can have both *abstract* and nonabstract methods.
* The first concrete class to extend an *abstract* class must implement all of its *abstract* methods.

### Interface Implementation
* Interfaces are contracts for what a calss can do, but they say nothing about the way in which the class must do it.
* Interfaces can be implemented bu any class, from any inheritance tree.
* An interface is like a 100-percent #abstract# class and is implicitly abstract whether you type the #abstract# modifier in the declaration or not.
* An interface can have only #abstract# methods, no concrete methods allowed.
* Interfaces methods are by default #public# and and #abstract# -- explicit declaration of these modifiers is optional.
* Interfaces can have constants, which are always implicitly #public#, #static#, # and #final#.
* Interface constant declaration of #public#, #static#, and #final# are optional in any combination.
*A legal nonabstract implementing class has the following properties:
	* It provides concrete implementations for the interface's methods.
	* It must follow all legal override rules for the methods it implements.
	* It must not declare any new checked exceptions for an implementation method.
	* It must not declare any checked exceptions that are broader that the exceptions declared in the interface method.
	* It may declare runtime exceptions on any interface method implementation regradless of the interface declaration.
	* It must maintain the exact signature (allowing for covariant returns) and return type of the methods it implements (but does not have to declare the exceptions of the interface).
* A class implementing an interface cen itself be #abstract#
* An #abstract# iplementing class does not have to implement the interface methods (but the first concrete subclass must)
* A class can extend only one class (no multiple inheritance), but it can implement many interfaces.
* Interfaces can extend one or more other interfaces.
* Interfaces cannot extend a class or implmement a class or interface.
* When taking the exam, verify that interface and calss declarations are legal before verifying other code logic.
